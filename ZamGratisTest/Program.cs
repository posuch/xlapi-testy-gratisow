﻿using cdn_api;

namespace ZamGratisTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int xlSesjaId = 0;
            int xlWersjaApi = 20193;
            XLLoginInfo_20193 xlLogin = new XLLoginInfo_20193();
            XLLogoutInfo_20193 xlLogout = new XLLogoutInfo_20193();
            XLDokumentZamNagInfo_20193 xlZamNag = new XLDokumentZamNagInfo_20193();
            XLZamkniecieDokumentuZamInfo_20193 xlZamkniecieZamNag = new XLZamkniecieDokumentuZamInfo_20193();
            XLDokumentZamElemInfo_20193 xlZamElem;
            int xlDokumentId=0;
            int xlWynik;

            xlLogin.Wersja = xlWersjaApi;
            xlLogin.ProgramID = "ZamTest";
            xlLogin.Baza = "Florentyna";
            xlLogin.OpeIdent = "operator";
            xlLogin.OpeHaslo = "haslo";

            xlLogout.Wersja = xlWersjaApi;

            xlWynik = cdn_api.cdn_api.XLLogin(xlLogin, ref xlSesjaId);

            xlZamNag.Wersja = xlWersjaApi;
            xlZamNag.Typ = 6;
            xlZamNag.Akronim = "*****";
            xlZamNag.Opis = "test";
            xlWynik = cdn_api.cdn_api.XLNowyDokumentZam(xlSesjaId, ref xlDokumentId, xlZamNag);


            //Gratis
            xlZamElem = new XLDokumentZamElemInfo_20193();
            xlZamElem.Wersja = xlWersjaApi;
            xlZamElem.Ilosc = "1";
            xlZamElem.Towar = "A-VISTA";
            xlZamElem.Gratis = 1;
            xlWynik = cdn_api.cdn_api.XLDodajPozycjeZam(xlDokumentId, xlZamElem);
            //Cena
            xlZamElem = new XLDokumentZamElemInfo_20193();
            xlZamElem.Wersja = xlWersjaApi;
            xlZamElem.Ilosc = "1";
            xlZamElem.Towar = "A-VISTA";
            xlZamElem.Gratis = 0;
            xlZamElem.Wartosc = "5.55";
            xlWynik = cdn_api.cdn_api.XLDodajPozycjeZam(xlDokumentId, xlZamElem);

            xlZamkniecieZamNag.Wersja = xlWersjaApi;
            xlWynik = cdn_api.cdn_api.XLZamknijDokumentZam(xlDokumentId, xlZamkniecieZamNag);

            xlWynik = cdn_api.cdn_api.XLLogoutEx(xlLogout, xlSesjaId);
        }
    }
}
